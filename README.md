# fancy-data-generator

## Usage

### Initialization

#### Loading all the available dictionaries

````
FancyDataGenerator generator = new FancyDataGenerator();
````

#### Loading particular dictionaries only

This approach will save memory in case you know which dictionaries you might need.

````
FancyDataGenerator generator = new FancyDataGenerator(
    FancyDataGenerator.Dictionary.FirstNames, 
    FancyDataGenerator.Dictionary.LastNames
);
````

### Random name generation

````
FancyData name = generator.get(
    FancyDataGenerator.Dictionary.FirstNames, 
    FancyDataGenerator.Dictionary.LastNames
);
````

#### Formatting in a human-readable style 

````
name.readCase(true);
````

#### Formatting in `CamelCase` style 

````
name.camelCase();
````

#### Formatting in a `snake_case` style 

````
name.snakeCase();
````