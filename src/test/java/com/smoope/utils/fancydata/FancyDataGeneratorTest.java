package com.smoope.utils.fancydata;

import org.junit.Assert;
import org.junit.Test;

public class FancyDataGeneratorTest {

    private FancyDataGenerator fancyDataGenerator = new FancyDataGenerator();

    @Test
    public void original1Dictionary() {
        FancyData name = generate(FancyDataGenerator.Dictionary.Adjectives);

        Assert.assertTrue(name.readCase(true).length() > 0);
    }

    @Test
    public void original2Dictionary() {
        FancyData name = generate(FancyDataGenerator.Dictionary.Adjectives, FancyDataGenerator.Dictionary.FirstNames);
        String[] temp = name.readCase(true).split(" ");

        Assert.assertTrue(temp.length == 2);
    }

    @Test
    public void original3Dictionary() {
        FancyData name = generate(FancyDataGenerator.Dictionary.Adjectives, FancyDataGenerator.Dictionary.FirstNames,
                FancyDataGenerator.Dictionary.Corporate);
        String[] temp = name.readCase(true).split(" ");

        Assert.assertTrue(temp.length == 3);
    }

    @Test
    public void original4Dictionary() {
        FancyData name = generate(FancyDataGenerator.Dictionary.Adjectives, FancyDataGenerator.Dictionary.FirstNames,
                FancyDataGenerator.Dictionary.LastNames, FancyDataGenerator.Dictionary.Corporate);
        String[] temp = name.readCase(true).split(" ");

        Assert.assertTrue(temp.length == 4);

        System.out.println();
    }

    @Test
    public void snakeCase1Dictionary() {
        FancyData name = generate(FancyDataGenerator.Dictionary.Adjectives);

        Assert.assertTrue(name.snakeCase().length() > 0);
    }

    @Test
    public void snakeCase2Dictionary() {
        FancyData name = generate(FancyDataGenerator.Dictionary.Adjectives, FancyDataGenerator.Dictionary.FirstNames);
        String[] temp = name.snakeCase().split("_");

        Assert.assertTrue(temp.length == 2);
    }

    @Test
    public void snakeCase2DictionaryMultiple() {
        FancyData name = generate(FancyDataGenerator.Dictionary.Adjectives, FancyDataGenerator.Dictionary.FirstNames);
        String[] temp = name.snakeCase().split("_");

        Assert.assertTrue(temp.length == 2);

        temp = name.readCase(true).split(" ");

        Assert.assertTrue(temp.length == 2);
    }

    @Test
    public void snakeCase3Dictionary() {
        FancyData name = generate(FancyDataGenerator.Dictionary.Adjectives, FancyDataGenerator.Dictionary.FirstNames,
                FancyDataGenerator.Dictionary.Corporate);
        String[] temp = name.snakeCase().split("_");

        Assert.assertTrue(temp.length == 3);
    }

    @Test
    public void snakeCase4Dictionary() {
        FancyData name = generate(FancyDataGenerator.Dictionary.Adjectives, FancyDataGenerator.Dictionary.FirstNames,
                FancyDataGenerator.Dictionary.LastNames, FancyDataGenerator.Dictionary.Corporate);
        String[] temp = name.snakeCase().split("_");

        Assert.assertTrue(temp.length == 4);
    }

    @Test
    public void email() {
        FancyData name = generate(FancyDataGenerator.Dictionary.Adjectives, FancyDataGenerator.Dictionary.FirstNames);
        String[] temp = name.email("some.com").split("@");

        Assert.assertTrue(temp.length == 2);
        Assert.assertTrue(temp[1].equals("some.com"));
    }

    private FancyData generate(FancyDataGenerator.Dictionary... dictionary) {
        return fancyDataGenerator.get(dictionary);
    }
}
