package com.smoope.utils.fancydata;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FancyData {

    private List<String> value;

    private enum Action {
        Capitalize, UpperCase, LowerCase
    }

    private FancyData() {
        this(new ArrayList<String>());
    }

    public FancyData(final List<String> value) {
        this.value = value;
    }

    public String readCase(boolean capitalize) {
        Action action = capitalize ? Action.Capitalize : Action.LowerCase;

        return StringUtils.join(prepare(action), " ");
    }

    public String camelCase() {
        return StringUtils.join(prepare(Action.Capitalize), "");
    }

    public String upperCase(final String delimiter) {
        return StringUtils.join(prepare(Action.UpperCase), delimiter);
    }

    public String upperCase() {
        return StringUtils.join(prepare(Action.UpperCase), "");
    }

    public String lowerCase(final String delimiter) {
        return StringUtils.join(prepare(Action.LowerCase), delimiter);
    }

    public String lowerCase() {
        return StringUtils.join(prepare(Action.LowerCase), "");
    }

    public String snakeCase() {
        return lowerCase("_");
    }

    public String email(final String domain) {
        return String.format("%s@%s", lowerCase(".").replace(" ", ""), domain);
    }

    public String[] parts() {
        return value.toArray(new String[value.size()]);
    }

    private List<String> prepare(Action action) {
        List<String> result = new ArrayList<String>(this.value.size());

        Iterator<String> iter = value.iterator();
        while (iter.hasNext()) {
            switch (action) {
                case Capitalize:
                    result.add(StringUtils.capitalize(iter.next()));
                    break;
                case LowerCase:
                    result.add(iter.next().toLowerCase());
                    break;
            }
        }

        return result;
    }
}
