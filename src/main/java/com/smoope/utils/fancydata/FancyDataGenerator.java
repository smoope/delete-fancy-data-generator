package com.smoope.utils.fancydata;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class FancyDataGenerator {

    private List<String> adjectives;

    private List<String> firstNames;

    private List<String> lastNames;

    private List<String> corporate;

    private List<String> sentences;

    public FancyDataGenerator() {
        this(Dictionary.Adjectives, Dictionary.FirstNames, Dictionary.LastNames, Dictionary.Corporate,
                Dictionary.Sentences);
    }

    public FancyDataGenerator(Dictionary... dictionary) {
        for (Dictionary dict: dictionary) {
            loadDictionary(dict);
        }
    }

    public FancyData get(Dictionary... dictionary) {
        return new FancyData(buildResults(dictionary));
    }

    private List<String> buildResults(Dictionary... dictionary) {
        List<String> result = new ArrayList<String>(dictionary.length);

        for (Dictionary dict: dictionary) {
            switch (dict) {
                case Adjectives:
                    result.add(randomValue(adjectives));
                    break;
                case FirstNames:
                    result.add(randomValue(firstNames));
                    break;
                case LastNames:
                    result.add(randomValue(lastNames));
                    break;
                case Corporate:
                    result.add(randomValue(corporate));
                    break;
                case Sentences:
                    result.add(randomValue(sentences));
                    break;
            }
        }

        return result;
    }

    private void loadDictionary(Dictionary dictionary) {
        switch (dictionary) {
            case Adjectives:
                adjectives = load("adjectives");
                break;
            case FirstNames:
                firstNames = load("firstNames");
                break;
            case LastNames:
                lastNames = load("lastNames");
                break;
            case Corporate:
                corporate = load("corporate");
                break;
            case Sentences:
                sentences = load("sentences");
                break;
        }
    }

    private List<String> load(final String filename) {
        List<String> result = new ArrayList<String>();

        try {
            ClassLoader classLoader = FancyDataGenerator.class.getClassLoader();
            BufferedReader r = new BufferedReader(
                    new InputStreamReader(classLoader.getResourceAsStream(String.format("%s.txt", filename)), "UTF8")
            );

            try {
                String l;
                while ((l = r.readLine()) != null)
                    result.add(l);
            } finally {
                r.close();
            }
        }
        catch (IOException exc) {

        }

        return result;
    }

    private String randomValue(List<String> dictionary) {
        return dictionary.get(ThreadLocalRandom.current().nextInt(0, dictionary.size()));
    }

    public enum Dictionary {
        Adjectives, FirstNames, LastNames, Corporate, Sentences
    }
}
